*** Settings ***
Library     SeleniumLibrary
Resource    Keywords.robot
Resource    Variables.robot


*** Variables ***
${MONEY_WITHDRAW}          320
${MONEY_DEPOSIT}           320


***Test Cases ***
User should be able to sign up an account
    Open Website
    Click Signup button
    Validate Username cannot blank
    Validate Username cannot contain whitespaces
    Validate Password cannot be less than 8 characters
    Validate Password cannot be larger than 32 characters
    Validate assword must contain numbers
    Validate Password must contain uppercase letters
    Success Signup          ${USERNAME}         ${VALID_PASSWOR}


User should be able to withdraw money 
    Open Website
    Click Signup button
    Success Signup                    ${USERNAME}         ${VALID_PASSWOR}
    Go to Withdraw Page
    Input Withdraw                    ${MONEY_WITHDRAW} 
    Check Transaction fee is 30% of the intended withdrawal is     ${MONEY_WITHDRAW} 
    Check final Withdraw is           ${MONEY_WITHDRAW} 
    Click Confirm Withdraw Button
    Check Transaction Withdraw Display Correctly of       ${MONEY_WITHDRAW}

User should be able to deposit money
    Open Website
    Click Signup button
    Success Signup                    ${USERNAME}         ${VALID_PASSWOR}
    Go to Deposit Page
    Input Deposit                     ${MONEY_DEPOSIT} 
    Check Transaction fee is 30% of the intended deposit is     ${MONEY_DEPOSIT} 
    Check Final Deposit of            ${MONEY_DEPOSIT} 
    Click Confirm Deposit Button
    Check Transaction Deposit Display Correctly of       ${MONEY_DEPOSIT} 

Account balance should update every 10 seconds 
    Open Website
    Click Signup button
    Success Signup                  ${USERNAME}         ${VALID_PASSWOR}
    Go to Deposit Page
    Input Deposit                   ${MONEY_DEPOSIT} 
    Check Transaction fee is 30% of the intended deposit is     ${MONEY_DEPOSIT} 
    Check Final Deposit of          ${MONEY_DEPOSIT} 
    Click Confirm Deposit Button
    Check balance should update on 10 seconds 