*** Variables ***
${WEB_URL}              https://anylogi-recruitment.web.app
${SIGNUP_BUTTON}        //button[contains(text(), 'SIGNUP')]
${LOGIN_BUTTON}         //button[contains(text(), 'LOGIN')]
${INPUT_USERNAME}       //input[1]
${INPUT_PASSWORD}       //input[2]


${CANNOT_BLANK_TEXT}                User name cannot be blank
${CANNOT_WHITESPACE_TEXT}           User name cannot contain whitespaces
${PASSWORD_LESS8_TEXT}              Password cannot be less than 8 characters
${PASSWORD_LONG32_TEXT}             Password cannot be longer than 32 characters
${PASSWORD_CONTAIN_NUMBER_TEXT}     Password must contain numbers
${PASSWORD_CONTAIN_UPPER_TEXT}      Password must contain uppercase letters



${PASSWORD_LESS8}           Pass@1
${PASSWORD_MORE32}          Pass@123Pass@123Pass@123Pass@123
${PASSWORD_ONLYTEXT}        Pass@Pass
${PASSWORD_NOUPPER}         pass12345

${VALID_PASSWOR}            Pass@1234
${USERNAME}                 Test



${DIPOSIT_BUTTON}       //a[contains(text(), 'デポジット')]
${WITHDRAW_BUTTON}      //a[contains(text(), '引き出す')]

${WITHDRAW_TEXT}      AFTER_TRANSACTION_FINAL_WITHDRAWAL
${INPUT_WITHDRAW}     //input
${WITHDRAW_CONFIRM_BUTTON}     //button[contains(text(), 'Withdraw')]
 


${DEPOSIT_TEXT}      AFTER_TRANSACTION_FINAL_DEPOSIT
${INPUT_DEPOSIT}     //input
${DEPOSIT_CONFIRM_BUTTON}     //button[contains(text(), 'Deposit')]
${TRANSACTION_FEE}   //span[contains(text(),"取引料金")]    