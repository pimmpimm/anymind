*** Keywords ***
Open Website
    Open Browser    ${WEB_URL}     chrome

Click Signup button
    Click Element                       ${SIGNUP_BUTTON}
    Element Should Not Be Visible       ${LOGIN_BUTTON}    timeout=3s

Validate Username cannot blank
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           ${EMPTY}
    Click Element                   ${SIGNUP_BUTTON}
    Wait Until Page Contains        ${CANNOT_BLANK_TEXT}        timeout=3s

Validate Username cannot contain whitespaces
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           ${SPACE}
    Click Element                   ${SIGNUP_BUTTON}
    Wait Until Page Contains        ${CANNOT_WHITESPACE_TEXT}       timeout=3s

Validate Password cannot be less than 8 characters
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           test
    Clear Element Text              ${INPUT_PASSWORD} 
    Input text                      ${INPUT_PASSWORD}           ${PASSWORD_LESS8}
    Click Element                   ${SIGNUP_BUTTON}
    Wait Until Page Contains        ${PASSWORD_LESS8_TEXT}       timeout=3s

Validate Password cannot be larger than 32 characters
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           test
    Clear Element Text              ${INPUT_PASSWORD} 
    Input text                      ${INPUT_PASSWORD}           ${PASSWORD_MORE32}
    Click Element                   ${SIGNUP_BUTTON}
    Wait Until Page Contains        ${PASSWORD_LONG32_TEXT}       timeout=3s

Validate assword must contain numbers
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           test
    Clear Element Text              ${INPUT_PASSWORD} 
    Input text                      ${INPUT_PASSWORD}           ${PASSWORD_ONLYTEXT}
    Click Element                   ${SIGNUP_BUTTON}
    Wait Until Page Contains        ${PASSWORD_CONTAIN_NUMBER_TEXT}       timeout=3s

Validate Password must contain uppercase letters
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           test
    Clear Element Text              ${INPUT_PASSWORD} 
    Input text                      ${INPUT_PASSWORD}           ${PASSWORD_NOUPPER}
    Click Element                   ${SIGNUP_BUTTON}
    Wait Until Page Contains        ${PASSWORD_CONTAIN_UPPER_TEXT}       timeout=3s 

Success Signup
    [Arguments]                     ${User}    ${Pass}
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           ${User}
    Clear Element Text              ${INPUT_PASSWORD} 
    Input text                      ${INPUT_PASSWORD}           ${Pass}
    Click Element                   ${SIGNUP_BUTTON}
    Wait Until Page Contains        Last updated:      timeout=3s 


Success Login 
    [Arguments]                     ${User}    ${Pass}
    Clear Element Text              ${INPUT_USERNAME} 
    Input text                      ${INPUT_USERNAME}           ${User}
    Clear Element Text              ${INPUT_PASSWORD} 
    Input text                      ${INPUT_PASSWORD}           ${Pass}
    Click Element                   ${LOGIN_BUTTON}
    Wait Until Page Contains        Last updated:

Go to Withdraw Page
    Click Element                   ${WITHDRAW_BUTTON}
    Wait Until Page Contains        ${WITHDRAW_TEXT}      timeout=3s 

Go to Deposit Page
    Click Element                   ${DIPOSIT_BUTTON} 
    Wait Until Page Contains        ${DEPOSIT_TEXT}      timeout=3s 

Input Withdraw
    [Arguments]                         ${Withdraw}
    Wait Until Element Is Enabled       ${INPUT_WITHDRAW}         timeout=3s 
    Input Text                          ${INPUT_WITHDRAW}         ${Withdraw}
    Click Element                       ${TRANSACTION_FEE}

Check Transaction fee is 30% of the intended withdrawal is 
    [Arguments]                         ${Withdraw}
    Wait Until Element Is Enabled       ${TRANSACTION_FEE}         timeout=3s
    ${Transaction}=                     Evaluate             ${Withdraw}*(30/100)
    Log To Console                      Transaction fee is ${Transaction}
    Wait Until Element Is Visible       //span[contains(text(), "${Transaction}0")]

Check final Withdraw is
    [Arguments]                         ${Withdraw}
    Wait Until Element Is Enabled       ${TRANSACTION_FEE}         timeout=3s
    ${Transaction_fee}=                 Evaluate             ${Withdraw}*(30/100)
    Log To Console                      Transaction fee is ${Transaction_fee}
    ${final_withdraw}=                  Evaluate        ${Withdraw}-${Transaction_fee}
    Log To Console                      Final withdraw is ${final_withdraw}
    Wait Until Element Is Visible       //span[contains(text(), "${final_withdraw}0")]

Click Confirm Withdraw Button
    Wait Until Element Is Enabled       ${WITHDRAW_CONFIRM_BUTTON}         timeout=3s
    Click Element                       ${WITHDRAW_CONFIRM_BUTTON} 
    Wait Until Page Contains            Last updated:

Input Deposit
    [Arguments]                     ${Deposit}
    Wait Until Element Is Enabled       ${INPUT_DEPOSIT}        timeout=3s 
    Input Text                          ${INPUT_DEPOSIT}        ${Deposit}

Check Transaction fee is 30% of the intended deposit is
    [Arguments]                         ${deposit}
    Wait Until Element Is Enabled       ${TRANSACTION_FEE}         timeout=3s
    ${Transaction}=                     Evaluate             ${deposit}*(30/100)
    Log To Console                      Transaction fee is ${Transaction}
    Wait Until Element Is Visible       //span[contains(text(), "${Transaction}0")]

Check Final Deposit of
    [Arguments]                         ${deposit}
    Wait Until Element Is Enabled       ${TRANSACTION_FEE}         timeout=3s
    ${Transaction_fee}=                 Evaluate                ${deposit}*(30/100)
    Log To Console                      Transaction fee is ${Transaction_fee}
    ${final_deposit}=                   Evaluate            ${deposit}-${Transaction_fee}
    Log To Console                      Final withdraw is ${final_deposit}
    Wait Until Element Is Visible       //span[contains(text(), "${final_deposit}0")]

Click Confirm Deposit Button
    Wait Until Element Is Enabled       ${DEPOSIT_CONFIRM_BUTTON}         timeout=3s
    Click Element                       ${DEPOSIT_CONFIRM_BUTTON} 
    Wait Until Page Contains            Last updated:

Check Transaction Withdraw Display Correctly of 
    [Arguments]                         ${Withdraw}
    ${Before_update}      Get Text            //tr[td[contains(text(), "Balance")]]//td[2]
    Log To Console        balance before update is ${Before_update}
    Wait Until Element Is Enabled       ${TRANSACTION_FEE}         timeout=3s
    ${Transaction_fee}=                 Evaluate             ${Withdraw}*(30/100)
    Log To Console                      Transaction fee is ${Transaction_fee}
    ${final_withdraw}=                  Evaluate        ${Withdraw}-${Transaction_fee}
    Log To Console                      Final withdraw is ${final_withdraw}
    ${balance}=           Evaluate        ${Before_update}-${final_withdraw}
    sleep         10s
    ${After_update}        Get Text            //tr[td[contains(text(), "Balance")]]//td[2]
    Should Be Equal	        ${After_update}    ${balance}

Check Transaction Deposit Display Correctly of 
    [Arguments]                         ${deposit}
    ${Before_update}      Get Text            //tr[td[contains(text(), "Balance")]]//td[2]
    Log To Console        balance before update is ${Before_update}
    ${Transaction_fee}=                 Evaluate            ${deposit}*(30/100)
    ${final_deposit}=                   Evaluate            ${deposit}-${Transaction_fee}
    ${balance}=           Evaluate        ${Before_update}+${final_deposit}
    sleep         10s
    ${After_update}        Get Text            //tr[td[contains(text(), "Balance")]]//td[2]
    Should Be Equal	        ${After_update}    ${balance}0
    Log To Console        balance after update is ${After_update}

Check balance should update on 10 seconds 
    ${Before_update}        Get Text            //tr[td[contains(text(), "Balance")]]//td[2]
    Log To Console        balance before update is ${Before_update}
    sleep         10s
    ${After_update}        Get Text            //tr[td[contains(text(), "Balance")]]//td[2]
    Log To Console        balance after update is ${After_update}
